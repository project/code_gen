<?php
// $Id:
define('TEMPLATES_PATH', drupal_get_path('module', 'code_gen') .'/templates/views');
$field_defs = array();

function code_gen_admin() {
  $content = '<div id="code_gen_create_views_definitions_form">';
    $content .= drupal_get_form('code_gen_create_views_definitions_form');
  $content .= '</div>';
  return $content;
}

function code_gen_create_views_definitions_form(&$form_state) {
  $form = array();
  $form['module_name'] = array(
    "#type" => "textfield",
    "#title" => t( "Module name" ),
    "#description" => t( "Name of module to auto generate." ),
    '#required' => TRUE,
  );
  $tables = _code_gen_get_table_names();
  $form['code_gen_tables'] = array(
    "#type" => "select",
    "#multiple" => true,
    "#title" => t( "Create view definitions for the following tables" ),
    "#options" => $tables,
    "#description" => t( "The selected tables will be analyzed and views definitions will be made." ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );
  return $form;
}
function code_gen_create_views_definitions_form_validate($form, &$form_state) {
  $do_not_create_yet = file_directory_path()."/".$form_state['values']['module_name'];
  if (file_check_directory($do_not_create_yet, !FILE_CREATE_DIRECTORY)) {
    form_set_error('path ', t('Directory already exists. System must be able to create new path at #full_path', array('#full_path' => $do_not_create_yet)));
  }
}

function code_gen_create_views_definitions_form_submit($form, &$form_state) {
  global $field_defs;
  global $join_defs;
  file_scan_directory(TEMPLATES_PATH, '^field.[A-Za-z]+.template$', array('.','..','CVS'), '_code_gen_pass_fields', FALSE);
  file_scan_directory(TEMPLATES_PATH, '^join.[A-Za-z]+.template$', array('.','..','CVS'), '_code_gen_pass_joins', FALSE);

  $module_name = $form_state['values']['module_name'];

  //Write the standard module files for 
  $file_mime = "text/plain";
  $path = file_directory_path()."/".$module_name;
  if (!file_check_directory($path, FILE_CREATE_DIRECTORY)) {
    form_set_error('path', '"'.$path.t('" is not a valid file path. System must be able to write to #full_path', array('#full_path' => $path)));
    return;
  }
  //LICENSE.txt
  _code_gen_copy_templates("LICENSE.txt",$path, "LICENSE.txt");
  //CHANGELOG.txt
  _code_gen_copy_templates("CHANGELOG.txt",$path, "CHANGELOG.txt", array("%module_name" => $module_name));
  //README.txt
  _code_gen_copy_templates("README.txt",$path, "README.txt", array("%module_name" => $module_name));
  //modulename.install
  _code_gen_copy_templates("install.template",$path, $module_name.".install", array("%module_name" => $module_name));
  //modulename.module
  _code_gen_copy_templates("module.template",$path, $module_name.".module", array("%module_name" => $module_name));
  //modulename.info
  _code_gen_copy_templates("info.template",$path, $module_name.".info", array("%module_name" => $module_name, "%table_names" => implode(", ",$form_state['values']['code_gen_tables'])));
  //Write the views.inc file
  $result = "";
  foreach ($form_state['values']['code_gen_tables'] as $table_name) {
    $result .= " // Definitions for ".$table_name."\n";
    $result .= "  \$data['".$table_name."']['table']['group'] = t('".strtr($table_name, array("_" => " "))."');\n";
    $schema = drupal_get_schema($table_name);
    if ($schema) {
      $result .= "  \$".$table_name." = drupal_get_schema('".$table_name."');\n";
      foreach ($schema['fields'] as $field_name => $field_values) {
        $result .= "  //".$field_values['type']."\n";
        $field_str = $field_defs[$field_values['type']];
        $result .= strtr($field_str, array("%table_name" => $table_name, "%field_name" => $field_name));
        if ($join_defs[$field_name]) {
          $result .= strtr($join_defs[$field_name], array("%table_name" => $table_name, "%field_name" => $field_name));
        }
      }
    } else {
      //OOps - this must be a db view rather than a table!
      //Use describe in stead of drupal_get_schema - this will work for tables too but we will miss the posibility
      // Of using the column description as field help
      $fields = db_query("describe {".$table_name."}");
      while ( $field_values = db_fetch_array($fields) ) {
        $field_name = $field_values['Field'];
        $field_type = substr($field_values['Type'], 0, strlen($field_values['Type']) - strlen(strstr($field_values['Type'], "(")));
        $result .= "  //".$field_type."\n";
        $field_str = strtr($field_defs[$field_type], array("\$%table_name['fields']['%field_name']['description']" => "'".$table_name." : ".$field_name."'"));
        $result .= strtr($field_str, array("%table_name" => $table_name, "%field_name" => $field_name));
        if ($join_defs[$field_name]) {
          $result .= strtr($join_defs[$field_name], array("%table_name" => $table_name, "%field_name" => $field_name));
        }
      }
    }


  }
  $path .= "/"."views";
  if (!file_check_directory($path, FILE_CREATE_DIRECTORY)) {
    form_set_error('path', '"'.$path.t('" is not a valid file path. System must be able to write to #full_path', array('#full_path' => $path)));
    return;
  }
  _code_gen_copy_templates("views.inc.template",$path, $module_name.".views.inc", array("%module_name" => $module_name, "%views_definitions" => $result));
}

/**
 * Get the list of table names.
 */
function _code_gen_get_table_names( ) {
  $out = "";
  // get auto_increment values and names of all tables
  $tables = db_query("show table status");
  while ( $table = db_fetch_array($tables) ) {
//    if(!empty($table['Engine'])) {
      $out[$table['Name']] = $table['Name'];
//    }
  }
  return $out;
}

/**
 * Get the list of db view names.
 */
function _code_gen_get_view_names() {
  $out = "";
  // Get auto_increment values and names of all views.
  $tables = db_query("show table status");
  while ( $table = db_fetch_array($tables) ) {
    if(empty($table['Engine'])) {
      $out[$table['Name']] = $table['Name'];
    }
  }
  return $out;
}

/**
 * Copy the template files and make the appropriate string substitutions
 */
function _code_gen_copy_templates($src_file_name, $dst_file_path, $dst_file_name, $substitutions = array()) {
  $template = file_get_contents(TEMPLATES_PATH."/".$src_file_name);
  $template_file = tempnam(file_directory_temp(), 'tmp_');
  if ($dst = fopen($template_file, "w")) {
    fwrite($dst, strtr($template, $substitutions));
    fclose($dst);
  }
  rename($template_file, $dst_file_path."/".$dst_file_name);
}

/**
 * Pass templates for known db field types
 */
function _code_gen_pass_fields($file_name) {
  global $field_defs; 
  $db_type = substr($file_name,
                    strlen(TEMPLATES_PATH."/field."),
                    strlen($file_name) - strlen(TEMPLATES_PATH."/field.") - strlen(".template"));
  $field_defs[$db_type] = file_get_contents($file_name);
}

/**
 * Pass templates for known join types
 */
function _code_gen_pass_joins($file_name) {
  global $join_defs;
  $field_name = substr($file_name,
                       strlen(TEMPLATES_PATH."/join."),
                       strlen($file_name) - strlen(TEMPLATES_PATH."/join.") - strlen(".template"));
  $join_defs[$field_name] = file_get_contents($file_name);
}
